#!/bin/bash

#mkdir -v $LFS/sources
#chmod -v a+wt $LFS/sources
#wget --input-file=wget-list --continue --directory-prefix=$LFS/sources
#rm gcc-7.3.0.tar.gz
#wget ftp.gnu.org/gnu/gcc/gcc-8.1.0/gcc-8.1.0.tar.gz
#mkdir -v $LFS/tools
#ln -sv $LFS/tools /
#yeah yeah blah blah
#At this point we assume you have done chapters 1-4

#Chapter 5
cd $LFS/sources
tar xf binutils-2.30.tar.gz
cd binutils-2.30
mkdir -v build
cd build
../configure --prefix=/tools            \
             --with-sysroot=$LFS        \
             --with-lib-path=/tools/lib \
             --target=$LFS_TGT          \
             --disable-nls              \
             --disable-werror
make -j4
case $(uname -m) in
  x86_64) mkdir -v /tools/lib && ln -sv lib /tools/lib64 ;;
esac
make install

cd $LFS/sources
tar xf gcc-8.1.0.tar.gz
cd gcc-8.1.0

tar -xf ../mpfr-4.0.1.tar.xz
cp -v mpfr-4.0.1 mpfr
tar -xf ../gmp-6.1.2.tar.xz
cp -v gmp-6.1.2 gmp
tar -xf ../mpc-1.1.0.tar.gz
cp -v mpc-1.1.0 mpc

for file in gcc/config/{linux,i386/linux{,64}}.h
do
  cp -uv $file{,.orig}
  sed -e 's@/lib\(64\)\?\(32\)\?/ld@/tools&@g' \
      -e 's@/usr@/tools@g' $file.orig > $file
  echo '
#undef STANDARD_STARTFILE_PREFIX_1
#undef STANDARD_STARTFILE_PREFIX_2
#define STANDARD_STARTFILE_PREFIX_1 "/tools/lib/"
#define STANDARD_STARTFILE_PREFIX_2 ""' >> $file
  touch $file.orig
done

case $(uname -m) in
  x86_64)
    sed -e '/m64=/s/lib64/lib/' \
        -i.orig gcc/config/i386/t-linux64
 ;;
esac
mkdir -v build
cd build

../configure                                       \
    --target=$LFS_TGT                              \
    --prefix=/tools                                \
    --with-glibc-version=2.11                      \
    --with-sysroot=$LFS                            \
    --with-newlib                                  \
    --without-headers                              \
    --with-local-prefix=/tools                     \
    --with-native-system-header-dir=/tools/include \
    --disable-nls                                  \
    --disable-shared                               \
    --disable-multilib                             \
    --disable-decimal-float                        \
    --disable-threads                              \
    --disable-libatomic                            \
    --disable-libgomp                              \
    --disable-libmpx                               \
    --disable-libquadmath                          \
    --disable-libssp                               \
    --disable-libvtv                               \
    --disable-libstdcxx                            \
    --enable-languages=c,c++
make
make install

cd $LFS/sources
#VERSIONCHANGE_REQUIRE
tar xf linux-4.15.3.tar.xz
cd linux-4.15.3
make mrproper
make INSTALL_HDR_PATH=dest headers_install
cp -rv dest/include/* /tools/include

#CHECKCMD
cd $LFS/sources
tar xf glibc-2.27.tar.xz
cd glibc-2.27
mkdir -v build
cd build
../configure                             \
      --prefix=/tools                    \
      --host=$LFS_TGT                    \
      --build=$(../scripts/config.guess) \
      --enable-kernel=3.2             \
      --with-headers=/tools/include      \
      libc_cv_forced_unwind=yes          \
      libc_cv_c_cleanup=yes
make
make install

echo 'int main(){}' > dummy.c
$LFS_TGT-gcc dummy.c
readelf -l a.out | grep ': /tools'

rm -v dummy.c a.out

#CHECKCMD
cd $LFS/sources
cd gcc-8.1.0
cd build
../libstdc++-v3/configure           \
    --host=$LFS_TGT                 \
    --prefix=/tools                 \
    --disable-multilib              \
    --disable-nls                   \
    --disable-libstdcxx-threads     \
    --disable-libstdcxx-pch         \
    --with-gxx-include-dir=/tools/$LFS_TGT/include/c++/8.1.0
make
make install

cd $LFS/sources
cd binutils-2.30
cd build
CC=$LFS_TGT-gcc                \
AR=$LFS_TGT-ar                 \
RANLIB=$LFS_TGT-ranlib         \
../configure                   \
    --prefix=/tools            \
    --disable-nls              \
    --disable-werror           \
    --with-lib-path=/tools/lib \
    --with-sysroot
make
make install
make -C ld clean
make -C ld LIB_PATH=/usr/lib:/lib
cp -v ld/ld-new /tools/bin

cd $LFS/sources
cd gcc-8.1.0
cat gcc/limitx.h gcc/glimits.h gcc/limity.h > \
  `dirname $($LFS_TGT-gcc -print-libgcc-file-name)`/include-fixed/limits.h
for file in gcc/config/{linux,i386/linux{,64}}.h
do
  cp -uv $file{,.orig}
  sed -e 's@/lib\(64\)\?\(32\)\?/ld@/tools&@g' \
      -e 's@/usr@/tools@g' $file.orig > $file
  echo '
#undef STANDARD_STARTFILE_PREFIX_1
#undef STANDARD_STARTFILE_PREFIX_2
#define STANDARD_STARTFILE_PREFIX_1 "/tools/lib/"
#define STANDARD_STARTFILE_PREFIX_2 ""' >> $file
  touch $file.orig
done
case $(uname -m) in
  x86_64)
    sed -e '/m64=/s/lib64/lib/' \
        -i.orig gcc/config/i386/t-linux64
  ;;
esac
tar -xf ../mpfr-4.0.1.tar.xz
mv -v mpfr-4.0.1 mpfr
tar -xf ../gmp-6.1.2.tar.xz
mv -v gmp-6.1.2 gmp
tar -xf ../mpc-1.1.0.tar.gz
mv -v mpc-1.1.0 mpc
mkdir -v build
cd build
CC=$LFS_TGT-gcc                                    \
CXX=$LFS_TGT-g++                                   \
AR=$LFS_TGT-ar                                     \
RANLIB=$LFS_TGT-ranlib                             \
../configure                                       \
    --prefix=/tools                                \
    --with-local-prefix=/tools                     \
    --with-native-system-header-dir=/tools/include \
    --enable-languages=c,c++                       \
    --disable-libstdcxx-pch                        \
    --disable-multilib                             \
    --disable-bootstrap                            \
    --disable-libgomp
make -j4
make install
ln -sv gcc /tools/bin/cc
echo 'int main(){}' > dummy.c
cc dummy.c
readelf -l a.out | grep ': /tools'
rm -v dummy.c a.out
cd $LFS/sources

tar xf tcl8.6.8-src.tar.gz
cd tcl8.6.8-src
cd unix
./configure --prefix=/tools
make -j4
make install
chmod -v u+w /tools/lib/libtcl8.6.so
make install-private-headers
ln -sv tclsh8.6 /tools/bin/tclsh

cd $LFS/sources
tar xf expect5.45.4.tar.gz
cd expect5.45.4
cp -v configure{,.orig}
sed 's:/usr/local/bin:/bin:' configure.orig > configure
./configure --prefix=/tools       \
            --with-tcl=/tools/lib \
            --with-tclinclude=/tools/include
make -j4
make SCRIPTS="" install

cd $LFS/sources
tar xf dejagnu-1.6.1.tar.gz
cd dejagnu-1.6.1
./configure --prefix=/tools
make install
make check

cd $LFS/sources
tar xf m4-1.4.18.tar.xz
cd m4-1.4.18
./configure --prefix=/tools
make -j4
make check
make install

cd $LFS/sources
tar xf ncurses-6.1.tar.gz
cd ncurses-6.1
sed -i s/mawk// configure
./configure --prefix=/tools \
            --with-shared   \
            --without-debug \
            --without-ada   \
            --enable-widec  \
            --enable-overwrite
make -j4
make install

cd $LFS/sources
tar xf bash-4.4.18.tar.gz
cd bash-4.4.18
./configure --prefix=/tools --without-bash-malloc
make -j4
make tests
make install
ln -sv bash /tools/bin/sh

cd $LFS/sources
tar xf bison-3.0.4.tar.xz
cd bison-3.0.4
./configure --prefix=/tools
make -j4
make install

cd $LFS/sources
tar xf bzip2-1.0.6.tar.gz
cd bzip2-1.0.6
make -j4
make PREFIX=/tools install

cd $LFS/sources
tar xf coreutils-8.29.tar.xz
cd coreutils-8.29
./configure --prefix=/tools --enable-install-program=hostname
make -j4
make install

cd $LFS/sources
tar xf diffutils-3.6.tar.xz
cd diffutils-3.6
./configure --prefix=/tools
make -j4
make install

cd $LFS/sources
tar xf file-5.32.tar.gz
cd file-5.32
./configure --prefix=/tools
make -j4
make install

cd $LFS/sources
tar xf findutils-4.6.0
cd findutils-4.6.0
./configure --prefix=/tools
make -j4
make install

cd $LFS/sources
tar xf gawk-4.2.0.tar.xz
cd gawk-4.2.0
./configure --prefix/tools
make -j4
make install

cd $LFS/sources
tar xf gettext-0.19.8.1.tar.xz
cd gettext-0.19.8.1
cd gettext-tools
EMACS="no" ./configure --prefix=/tools --disable-shared
make -C gnulib-lib
make -C intl pluralx.c
make -C src msgfmt
make -C src msgmerge
make -C src xgettext
cp -v src/{msgfmt,msgmerge,xgettext} /tools/bin

cd $LFS/sources
tar xf grep-3.1.tar.xz
cd grep-3.1
./configure --prefix=/tools
make -j4
make install

cd $LFS/sources
tar xf gzip-1.9.tar.xz
cd gzip-1.9
./configure --prefix=/tools
make -j4
make install

cd $LFS/sources
tar xf make-4.2.1.tar.bz2
cd make-4.2.1
sed -i '211,217 d; 219,229 d; 232 d' glob/glob.c
./configure --prefix=/tools --without-guile
make -j4
make install

cd $LFS/sources
tar xf patch-2.7.6.tar.xz
cd patch-2.7.6
./configure --prefix=/tools
make
make install

#CHECK_VERSION
cd $LFS/sources
tar xf perl-5.26.1.tar.xz
cd perl-5.26.1
sh Configure -des -Dprefix=/tools -Dlibs=-lm
make -j4
cp -v perl cpan/podlators/scripts/pod2man /tools/bin
mkdir -pv /tools/lib/perl5/5.26.1
cp -Rv lib/* /tools/lib/perl5/5.26.1

cd $LFS/sources
tar xf sed-4.4.tar.xz
cd sed-4.4
./configure --prefix=/tools
make -j4
make install

cd $LFS/sources
tar xf tar-1.30.tar.xz
cd tar-1.30
./configure --prefix=/tools
make -j4
make install

cd $LFS/sources
tar xf texinfo-6.5.tar.xz
cd texinfo-6.5
./configure --prefix=/tools
make -j4
make install

cd $LFS/sources
tar xf util-linux-2.31.1.tar.xz
cd util-linux-2.31.1
./configure --prefix=/tools                \
            --without-python               \
            --disable-makeinstall-chown    \
            --without-systemdsystemunitdir \
            --without-ncurses              \
            PKG_CONFIG=""
make -j4
make install

cd $LFS/sources
tar xf xz-5.2.3.tar.xz
cd xz-5.2.3
./configure --prefix=/tools
make -j4
make install

# We are DONE with Chapter 5 temporary tools!
# To strip unecessary debug symbols:
strip --strip-debug /tools/lib/*
/usr/bin/strip --strip-unneeded /tools/{,s}bin/*

# Remove old tars:
rm *.tar.*

# Remove other "unneeded" files
find /tools/{lib,libexec} -name \*.la -delete

# Give ownership of /tools back to root
chown -R root:root $LFS/tools

