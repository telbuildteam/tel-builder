#!/bin/bash

cd /sources
tar xf libtool-2.4.6.tar.xz
cd libtool-2.4.6
./configure --prefix=/usr
make -j4
make TESTSUITEFLAGS=-j4 check
#5 tests are known to fail, please ignore
make install

cd /sources
tar xf gdbm-1.14.1.tar.*
cd gdbm-1.14.1
./configure --prefix=/usr \
            --disable-static \
            --enable-libgdbm-compat
make -j4
make check
make install

cd /sources
tar xf gperf-3.1.tar.*
./configure --prefix=/usr --docdir=/usr/share/doc/gperf-3.1
make -j4
make -j1 check
make install

cd /sources
tar xf expat-2.2.5.tar.*
cd expat-2.2.5
sed -i 's|usr/bin/env |bin/|' run.sh.in
./configure --prefix=/usr --disable-static
make -j4
make check
make install
install -v -dm755 /usr/share/doc/expat-2.2.5
install -v -m644 doc/*.{html,png,css} /usr/share/doc/expat-2.2.5

cd /sources
tar xf inetutils-1.9.4.tar.*
cd inetutils-1.9.4
./configure --prefix=/usr        \
            --localstatedir=/var \
            --disable-logger     \
            --disable-whois      \
            --disable-rcp        \
            --disable-rexec      \
            --disable-rlogin     \
            --disable-rsh        \
            --disable-servers
make -j4
make check
make install
mv -v /usr/bin/{hostname,ping,ping6,traceroute} /bin
mv -v /usr/bin/ifconfig /sbin

cd /sources
tar xf perl-5.26.1.tar.*
cd perl-5.26.1
echo "127.0.0.1 localhost $(hostname)" > /etc/hosts
export BUILD_ZLIB=False
export BUILD_BZIP2=0
sh Configure -des -Dprefix=/usr                 \
                  -Dvendorprefix=/usr           \
                  -Dman1dir=/usr/share/man/man1 \
                  -Dman3dir=/usr/share/man/man3 \
                  -Dpager="/usr/bin/less -isR"  \
                  -Duseshrplib                  \
                  -Dusethreads
make -j4
make -k test
make install
unset BUILD_ZLIB BUILD_BZIP2

cd /sources
tar xf XML-Parser-2.44.tar.*
cd XML-Parser-2.44
perl Makefile.PL
make -j4
make test
make install

cd /sources
tar xf intltool-0.51.0.tar.*
cd intltool-0.51.0
sed -i 's:\\\${:\\\$\\{:' intltool-update.in
./configure --prefix=/usr
make -j4
make check
make install
install -v -Dm644 doc/I18N-HOWTO /usr/share/doc/intltool-0.51.0/I18N-HOWTO

cd /sources
tar xf autoconf-2.69.tar.*
cd autoconf-2.69
./configure --prefix=/usr
make -j4
make TESTSUITEFLAGS=-j4 check

cd /sources
tar xf automake-1.15.1.tar.*
cd automake-1.15.1
./configure --prefix=/usr --docdir=/usr/share/doc/automake-1.15.1
make -j4
sed -i "s:./configure:LEXLIB=/usr/lib/libfl.a &:" t/lex-{clean,depend}-cxx.sh
make -j4 check
make install

cd /sources
tar xf xz-5.2.3.tar.*
cd xz-5.2.3
./configure --prefix=/usr    \
            --disable-static \
            --docdir=/usr/share/doc/xz-5.2.3
make -j4
make check
make install
mv -v   /usr/bin/{lzma,unlzma,lzcat,xz,unxz,xzcat} /bin
mv -v /usr/lib/liblzma.so.* /lib
ln -svf ../../lib/$(readlink /usr/lib/liblzma.so) /usr/lib/liblzma.so

cd /sources
tar xf kmod-25.tar.*
cd kmod-25
./configure --prefix=/usr          \
            --bindir=/bin          \
            --sysconfdir=/etc      \
            --with-rootlibdir=/lib \
            --with-xz              \
            --with-zlib
make -j4
make install

for target in depmod insmod lsmod modinfo modprobe rmmod; do
  ln -sfv ../bin/kmod /sbin/$target
done

ln -sfv kmod /bin/lsmod

cd /sources
tar xf gettext-0.19.8.1.tar.*
cd gettext-0.19.8.1
sed -i '/^TESTS =/d' gettext-runtime/tests/Makefile.in &&
sed -i 's/test-lock..EXEEXT.//' gettext-tools/gnulib-tests/Makefile.in
./configure --prefix=/usr    \
            --disable-static \
            --docdir=/usr/share/doc/gettext-0.19.8.1
make -j4
make check
make install

cd /sources
tar xf elfutils-0.170.tar.xz
cd elfutils-0.170
./configure --prefix=/usr
make -j4
make check
make -C libelf install
install -vm644 config/libelf.pc /usr/lib/pkgconfig

cd /sources
tar xf libffi-3.2.1.tar.*
cd libffi-3.2.1
sed -e '/^includesdir/ s/$(libdir).*$/$(includedir)/' \
    -i include/Makefile.in

sed -e '/^includedir/ s/=.*$/=@includedir@/' \
    -e 's/^Cflags: -I${includedir}/Cflags:/' \
    -i libffi.pc.in
./configure --prefix=/usr --disable-static
make -j4
make check
make install

cd /sources
tar xf openssl-1.1.0g
cd openssl-1.1.0g
./config --prefix=/usr         \
         --openssldir=/etc/ssl \
         --libdir=lib          \
         shared                \
         zlib-dynamic
make -j4
make test
sed -i '/INSTALL_LIBS/s/libcrypto.a libssl.a//' Makefile
make MANSUFFIX=ssl install
mv -v /usr/share/doc/openssl /usr/share/doc/openssl-1.1.0g
cp -vfr doc/* /usr/share/doc/openssl-1.1.0g

cd /sources
tar xf Python-3.6.5.tar.xz
cd Python-3.6.5
./configure --prefix=/usr       \
            --enable-shared     \
            --with-system-expat \
            --with-system-ffi   \
            --with-ensurepip=yes

make -j4
make install
chmod -v 755 /usr/lib/libpython3.6m.so
chmod -v 755 /usr/lib/libpython3.so
install -v -dm755 /usr/share/doc/python-3.6.4/html

tar --strip-components=1  \
    --no-same-owner       \
    --no-same-permissions \
    -C /usr/share/doc/python-3.6.4/html \
    -xvf ../python-3.6.4-docs-html.tar.bz2

cd /sources
tar xf ninja-1.8.2.tar.*
cd ninja-1.8.2
patch -Np1 -i ../ninja-1.8.2-add_NINJAJOBS_var-1.patch
python3 configure.py --bootstrap
python3 configure.py
./ninja ninja_test
./ninja_test --gtest_filter=-SubprocessTest.SetWithLots
install -vm755 ninja /usr/bin/
install -vDm644 misc/bash-completion /usr/share/bash-completion/completions/ninja
install -vDm644 misc/zsh-completion  /usr/share/zsh/site-functions/_ninja

cd /sources
tar xf meson-0.44.0.tar.*
cd meson-0.44.0
python3 setup.py build
python3 setup.py install

cd /sources
tar xf procps-ng-3.3.12.tar.*
./configure --prefix=/usr                            \
            --exec-prefix=                           \
            --libdir=/usr/lib                        \
            --docdir=/usr/share/doc/procps-ng-3.3.12 \
            --disable-static                         \
            --disable-kill
make -j4
sed -i -r 's|(pmap_initname)\\\$|\1|' testsuite/pmap.test/pmap.exp
sed -i '/set tty/d' testsuite/pkill.test/pkill.exp
rm testsuite/pgrep.test/pgrep.exp
make check
make install
mv -v /usr/lib/libprocps.so.* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libprocps.so) /usr/lib/libprocps.so

cd /sources
tar xf e2fsprogs-1.43.9.tar.*
cd e2fsprogs-1.43.9
mkdir -v build
cd build
LIBS=-L/tools/lib                    \
CFLAGS=-I/tools/include              \
PKG_CONFIG_PATH=/tools/lib/pkgconfig \
../configure --prefix=/usr           \
             --bindir=/bin           \
             --with-root-prefix=""   \
             --enable-elf-shlibs     \
             --disable-libblkid      \
             --disable-libuuid       \
             --disable-uuidd         \
             --disable-fsck
make -j4
ln -sfv /tools/lib/lib{blk,uu}id.so.1 lib
make LD_LIBRARY_PATH=/tools/lib check
make install
make install-libs
chmod -v u+w /usr/lib/{libcom_err,libe2p,libext2fs,libss}.a
gunzip -v /usr/share/info/libext2fs.info.gz
install-info --dir-file=/usr/share/info/dir /usr/share/info/libext2fs.info
makeinfo -o      doc/com_err.info ../lib/et/com_err.texinfo
install -v -m644 doc/com_err.info /usr/share/info
install-info --dir-file=/usr/share/info/dir /usr/share/info/com_err.info

cd /sources
cd coreutils-8.29
patch -Np1 -i ../coreutils-8.29-i18n-1.patch
sed -i '/test.lock/s/^/#/' gnulib-tests/gnulib.mk
FORCE_UNSAFE_CONFIGURE=1 ./configure \
            --prefix=/usr            \
            --enable-no-install-program=kill,uptime
FORCE_UNSAFE_CONFIGURE=1 make
make NON_ROOT_USERNAME=nobody check-root
echo "dummy:x:1000:nobody" >> /etc/group
chown -Rv nobody .
su nobody -s /bin/bash \
          -c "PATH=$PATH make RUN_EXPENSIVE_TESTS=yes check"
sed -i '/dummy/d' /etc/group
make install
mv -v /usr/bin/{cat,chgrp,chmod,chown,cp,date,dd,df,echo} /bin
mv -v /usr/bin/{false,ln,ls,mkdir,mknod,mv,pwd,rm} /bin
mv -v /usr/bin/{rmdir,stty,sync,true,uname} /bin
mv -v /usr/bin/chroot /usr/sbin
mv -v /usr/share/man/man1/chroot.1 /usr/share/man/man8/chroot.8
sed -i s/\"1\"/\"8\"/1 /usr/share/man/man8/chroot.8
mv -v /usr/bin/{head,sleep,nice} /bin

cd /sources
tar xf check-0.12.0.tar.*
cd check-0.12.0
./configure --prefix=/usr
make -j4
make check
make install

cd /sources
tar xf diffutils-3.6.tar.*
cd diffutils-3.6
./configure --prefix=/usr
make -j4
make check
make install

cd /sources
tar xf gawk-4.2.0.tar.*
sed -i 's/extras//' Makefile.in
./configure --prefix=/usr
make -j4
make check
make install
mkdir -v /usr/share/doc/gawk-4.2.0
cp    -v doc/{awkforai.txt,*.{eps,pdf,jpg}} /usr/share/doc/gawk-4.2.0

cd /sources
tar xf findutils-4.6.0.tar.*
cd findutils-4.6.0
sed -i 's/test-lock..EXEEXT.//' tests/Makefile.in
./configure --prefix=/usr --localstatedir=/var/lib/locate
make -j4
make check
make install
mv -v /usr/bin/find /bin
sed -i 's|find:=${BINDIR}|find:=/bin|' /usr/bin/updatedb

cd /sources
tar xf groff-1.22.3.tar.*
cd groff-1.22.3
PAGE=<paper_size> ./configure --prefix=/usr
make -j1
make install

cd /sources
tar xf grub-2.02.tar.*
cd grub-2.02
./configure --prefix=/usr          \
            --sbindir=/sbin        \
            --sysconfdir=/etc      \
            --disable-efiemu       \
            --disable-werror
make -j4
make install

cd /sources
tar xf less-530.tar.*
cd less-530
./configure --prefix=/usr --sysconfdir=/etc
make -j4
make install

cd /sources
tar xf gzip-1.9.tar.*
cd gzip-1.9
./configure --prefix=/usr
make -j4
make check
make install

#CHECK_VERSION
cd /sources
tar xf iproute2-4.15.0.tar.xz
cd iproute2-4.15.0
sed -i /ARPD/d Makefile
rm -fv man/man8/arpd.8
sed -i 's/m_ipt.o//' tc/Makefile
make -j4
make DOCDIR=/usr/share/doc/iproute2-4.15.0 install

cd /sources
tar xf kbd-2.0.4.tar.*
cd kbd-2.0.4
patch -Np1 -i ../kbd-2.0.4-backspace-1.patch
sed -i 's/\(RESIZECONS_PROGS=\)yes/\1no/g' configure
sed -i 's/resizecons.8 //' docs/man/man8/Makefile.in
PKG_CONFIG_PATH=/tools/lib/pkgconfig ./configure --prefix=/usr --disable-vlock
make -j4
make check
#LFS_NOTE: Apparently languages such as Belarusian are not installed right and users need to
#          download own keymaps. Keep in mind for future tech support issues.
make install
mkdir -v       /usr/share/doc/kbd-2.0.4
cp -R -v docs/doc/* /usr/share/doc/kbd-2.0.4

cd /sources
tar xf libpipeline-1.5.0.tar.*
cd libpipeline-1.5.0
./configure --prefix=/usr
make -j4
make check
make install

cd /sources
tar xf make-4.2.1.tar.*
cd make-4.2.1
sed -i '211,217 d; 219,229 d; 232 d' glob/glob.c
./configure --prefix=/usr
make -j4
make PERL5LIB=$PWD/tests/ check
make install

cd /sources
tar xf patch-2.7.6.tar.*
cd patch-2.7.6
./configure --prefix=/usr
make
make check
make install

cd /sources
tar xf sysklogd-1.5.1.tar.*
cd sysklogd-1.5.1
sed -i '/Error loading kernel symbols/{n;n;d}' ksym_mod.c
sed -i 's/union wait/int/' syslogd.c
make -j4
make BINDIR=/sbin install

cat > /etc/syslog.conf << "EOF"
# Begin /etc/syslog.conf

auth,authpriv.* -/var/log/auth.log
*.*;auth,authpriv.none -/var/log/sys.log
daemon.* -/var/log/daemon.log
kern.* -/var/log/kern.log
mail.* -/var/log/mail.log
user.* -/var/log/user.log
*.emerg *

# End /etc/syslog.conf
EOF

cd /sources
tar xf sysvinit-2.88dsf
cd sysvinit-2.88
patch -Np1 -i ../sysvinit-2.88dsf-consolidated-1.patch
make -C src -j4
make -C src install

cd /sources
tar xf eudev-3.2.5.tar.*
cd eudev-3.2.5
sed -r -i 's|/usr(/bin/test)|\1|' test/udev-test.pl
cat > config.cache << "EOF"
HAVE_BLKID=1
BLKID_LIBS="-lblkid"
BLKID_CFLAGS="-I/tools/include"
EOF
./configure --prefix=/usr           \
            --bindir=/sbin          \
            --sbindir=/sbin         \
            --libdir=/usr/lib       \
            --sysconfdir=/etc       \
            --libexecdir=/lib       \
            --with-rootprefix=      \
            --with-rootlibdir=/lib  \
            --enable-manpages       \
            --disable-static        \
            --config-cache
LIBRARY_PATH=/tools/lib make -j4
mkdir -pv /lib/udev/rules.d
mkdir -pv /etc/udev/rules.d
make LD_LIBRARY_PATH=/tools/lib check
make LD_LIBRARY_PATH=/tools/lib install
tar -xvf ../udev-lfs-20171102.tar.bz2
make -f udev-lfs-20171102/Makefile.lfs install

LD_LIBRARY_PATH=/tools/lib udevadm hwdb --update

cd /sources
tar xf util-linux-2.31.1.tar.*
cd util-linux-2.31.1
mkdir -pv /var/lib/hwclock
./configure ADJTIME_PATH=/var/lib/hwclock/adjtime   \
            --docdir=/usr/share/doc/util-linux-2.31.1 \
            --disable-chfn-chsh  \
            --disable-login      \
            --disable-nologin    \
            --disable-su         \
            --disable-setpriv    \
            --disable-runuser    \
            --disable-pylibmount \
            --disable-static     \
            --without-python     \
            --without-systemd    \
            --without-systemdsystemunitdir
make -j4
chown -Rv nobody .
su nobody -s /bin/bash -c "PATH=$PATH make -k check"
make install

cd /sources
tar xf man-db-2.8.1.tar.*
cd man-db-2.8.1
./configure --prefix=/usr                        \
            --docdir=/usr/share/doc/man-db-2.8.1 \
            --sysconfdir=/etc                    \
            --disable-setuid                     \
            --enable-cache-owner=bin             \
            --with-browser=/usr/bin/lynx         \
            --with-vgrind=/usr/bin/vgrind        \
            --with-grap=/usr/bin/grap            \
            --with-systemdtmpfilesdir=
make -j4
make check
make install

cd /sources
tar xf tar-1.30.tar.*
cd tar-1.30
FORCE_UNSAFE_CONFIGURE=1  \
./configure --prefix=/usr \
            --bindir=/bin
make -j4
make check
make install
make -C doc install-html docdir=/usr/share/doc/tar-1.30

cd /sources
tar xf texinfo-6.5.tar.*
cd texinfo-6.5
./configure --prefix=/usr --disable-static
make -j4
make check
make install
make TEXMF=/usr/share/texmf install-tex

pushd /usr/share/info
rm -v dir
for f in *
  do install-info $f dir 2>/dev/null
done
popd

cd /sources
tar xf vim-8.0.586.tar.*
cd vim-8.0.586
echo '#define SYS_VIMRC_FILE "/etc/vimrc"' >> src/feature.h
sed -i '/call/{s/split/xsplit/;s/303/492/}' src/testdir/test_recover.vim
./configure --prefix=/usr
make -j4
make -j1 test &> vim-test.log
make install
ln -sv vim /usr/bin/vi
for L in  /usr/share/man/{,*/}man1/vim.1; do
    ln -sv vim.1 $(dirname $L)/vi.1
done
ln -sv ../vim/vim80/doc /usr/share/doc/vim-8.0.586

cat > /etc/vimrc << "EOF"
" Begin /etc/vimrc

" Ensure defaults are set before customizing settings, not after
source $VIMRUNTIME/defaults.vim
let skip_defaults_vim=1

set nocompatible
set backspace=2
set mouse=
syntax on
if (&term == "xterm") || (&term == "putty")
  set background=dark
endif

" End /etc/vimrc
EOF

#We're done installing packages!
